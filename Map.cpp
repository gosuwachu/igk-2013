#include "StdAfx.h"
#include "Map.h"
#include "PathUtils.h"

Map::Map(int levelNumber, b2World* world)
{
    this->levelNumber = levelNumber;
    this->world = world;
}

Map::~Map(void)
{
}

Map* Map::create( int levelNumber, b2World* world )
{
    Map* map = new Map(levelNumber, world);
    map->init();
    return (Map*)map->autorelease();
}

bool Map::init()
{
    CCLayer::init();

    origin = CCPointZero;
    mapWidth = getContentSize().width;
    mapHeight = getContentSize().height;
    height = 1;
    width = 1;

    
    char fileName[256];
    sprintf(fileName, "map%d.bmp", levelNumber);
    loadMap(PathUtils::resourcePath(fileName).c_str());

    defineStaticWalls();

    background = CCSprite::create("bgkmap.jpg");
    background->setPosition(ccp(getContentSize().width / 2, getContentSize().height / 2));
    float scale = MAX(mapWidth / background->getContentSize().width,
        mapHeight / background->getContentSize().height);
    background->setScale(scale);
    addChild(background, -1);

    return true;
}

void trimr(char * buffer) {
    unsigned length = strlen(buffer);
    while(buffer[0] && (buffer[length-1] == '\n' || buffer[length-1] == '\r'))
        buffer[length-1] = 0;
}

const char* basename(const char* p) {
    if(strlen(p) >= 3)
        return &p[strlen(p)-3];
    return "";
}

#ifndef _WIN32
#pragma pack(push,1)
typedef uint32_t DWORD;
typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t LONG;

typedef struct tagBITMAPINFOHEADER {
    DWORD biSize;
    LONG  biWidth;
    LONG  biHeight;
    WORD  biPlanes;
    WORD  biBitCount;
    DWORD biCompression;
    DWORD biSizeImage;
    LONG  biXPelsPerMeter;
    LONG  biYPelsPerMeter;
    DWORD biClrUsed;
    DWORD biClrImportant;
} BITMAPINFOHEADER, *PBITMAPINFOHEADER;

typedef struct tagBITMAPFILEHEADER {
    WORD  bfType;
    DWORD bfSize;
    WORD  bfReserved1;
    WORD  bfReserved2;
    DWORD bfOffBits;
} BITMAPFILEHEADER, *PBITMAPFILEHEADER;

#define strcmpi(X,Y)    strcasecmp(X,Y)

typedef uint8_t byte;
#pragma pack(pop)
#endif

void Map::loadMap(const std::string& name)
{
    FILE* file = fopen(name.c_str(), "rb");
    if(!file)
        return;

    if(!strcmpi(basename(name.c_str()), "bmp")) {
        BITMAPFILEHEADER bmfh;
        fread(&bmfh,sizeof(BITMAPFILEHEADER),1,file);
        BITMAPINFOHEADER bmih;
        fread(&bmih,sizeof(BITMAPINFOHEADER),1,file);

        fseek(file, bmfh.bfOffBits, SEEK_SET);

        width = bmih.biWidth;
        height = bmih.biHeight;

        float blockWidth = getContentSize().width / width;
        float blockHeight = getContentSize().height / height;
        blockWidth = blockHeight = MIN(blockWidth, blockHeight);

        mapWidth = blockWidth * width;
        mapHeight = blockHeight * height;
        origin = ccp((getContentSize().width - mapWidth) / 2, (getContentSize().height - mapHeight) / 2);

        assert(bmih.biBitCount == 24);
        assert(bmih.biCompression == 0);

        unsigned pad = (LONG)((float)width*(float)bmih.biBitCount/8.0);
        unsigned byteWidth = pad;

        //add any extra space to bring each line to a DWORD boundary
        while(pad%4!=0) {
            pad++;
        }

        for(unsigned i = height; i-- > 0; ) {
            byte bytes[3];
            for(unsigned j = 0; j < width; ++j) {
                int index = (height - i - 1) * width + j;
                fread(bytes, 3, 1, file);
                std::swap(bytes[0], bytes[2]);
                /*if(bytes[0]>80 && bytes[1]>80 && bytes[2]>80)
                    continue;*/

                float x = origin.x + blockWidth * j;
                float y = origin.y + blockHeight * i;

                if(bytes[0] == 0) {
                    setBlockAt(x, y, blockWidth, blockHeight, BT_Brick);
                }
                else if(bytes[0] == 34) {
                    setBlockAt(x, y, blockWidth, blockHeight, BT_Plant);
                }
                else if(bytes[0] == 109) {
                    setBlockAt(x, y, blockWidth, blockHeight, BT_Stone);
                }
                else if(bytes[0] == 66) {
                    setBlockAt(x, y, blockWidth, blockHeight, BT_Wood);
                }
                else if(bytes[0] == 215) {
                    spawPoints.push_back(ccp(x + blockWidth / 2, y + blockHeight / 2));
                }

                CCLog("%d", bytes[0]);
            }
            fread(bytes, pad-byteWidth, 1, file);
        }
    }

    fclose(file);
}

void Map::setBlockAt( float x, float y, float blockWidth, float blockHeight, int type )
{
    if(type == BT_Plant) {
        blockHeight = blockWidth = blockWidth * 1.5 + CCRANDOM_0_1() * 20;
    }
    
	Block* block = Block::create((BlockType)type, x + blockWidth / 2, y + blockHeight / 2);
	block->setTag(5);
    // decrease block size so it play nicely with physics
    blockWidth -= 0.01;
    blockHeight -= 0.01;
	//block->setContentSize(CCSizeMake(blockWidth, blockHeight));
    block->setScaleX(blockWidth / block->getContentSize().width);
    block->setScaleY(blockHeight / block->getContentSize().height);
	block->setupPhisics(world);

    if(type == BT_Plant)
    {
        this->addChild(block, 1);
    }
    else
    {
        this->addChild(block);
    }
	
    this->blockChilds.push_back(block);
}

void Map::defineStaticWalls()
{
    // Define the ground body.
    b2BodyDef groundBodyDef;
    groundBodyDef.position.Set(0, 0); // bottom-left corner

    // Call the body factory which allocates memory for the ground body
    // from a pool and creates the ground box shape (also from a pool).
    // The body is also added to the world.
    b2Body* groundBody = world->CreateBody(&groundBodyDef);

    // Define the ground box shape.
    b2EdgeShape groundBox;

    CCPoint leftBottom = ccp(origin.x, origin.y);
    CCPoint rightBottom = ccp(origin.x + mapWidth, origin.y);
    CCPoint leftTop = ccp(origin.x, origin.y + mapHeight);
    CCPoint rightTop = ccp(origin.x + mapWidth, origin.y + mapHeight);

    // bottom
    groundBox.Set(b2Vec2(leftBottom.x/PTM_RATIO, leftBottom.y/PTM_RATIO), 
        b2Vec2(rightBottom.x/PTM_RATIO, rightBottom.y/PTM_RATIO));
    groundBody->CreateFixture(&groundBox,0);

    // top
    groundBox.Set(b2Vec2(leftTop.x/PTM_RATIO, leftTop.y/PTM_RATIO), 
        b2Vec2(rightTop.x/PTM_RATIO, rightTop.y/PTM_RATIO));
    groundBody->CreateFixture(&groundBox,0);

    // left
    groundBox.Set(b2Vec2(leftTop.x/PTM_RATIO, leftTop.y/PTM_RATIO), 
        b2Vec2(leftBottom.x/PTM_RATIO, leftBottom.y/PTM_RATIO));
    groundBody->CreateFixture(&groundBox,0);

    // right
    groundBox.Set(b2Vec2(rightBottom.x/PTM_RATIO,rightBottom.y/PTM_RATIO), 
        b2Vec2(rightTop.x/PTM_RATIO,rightTop.y/PTM_RATIO));
    groundBody->CreateFixture(&groundBox,0);
}
