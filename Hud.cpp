#include "StdAfx.h"
#include "Hud.h"
#include "PlayerManager.h"

Hud::Hud()
{
}

Hud::~Hud()
{
	unscheduleUpdate();
}

Hud* Hud::create()
{
	Hud *pRet = new Hud();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        CC_SAFE_DELETE(pRet);
        return NULL;
    }
}

void Hud::createHudForPlayer(Player* player)
{
	int playerIndex = player->m_playerId;
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	int startLeftPos = 10;

	/*CCLabelTTF* playerHeader = CCLabelTTF::create(CCString::createWithFormat("%d Player score: %d", player->m_playerId, player->score)->getCString(), "Verdana", 19);
	playerHeader->setAnchorPoint(ccp(0,1));
	playerHeader->setPosition(ccp(startLeftPos + 20*(playerIndex-1), winSize.height - 10));
	this->addChild(playerHeader);
	*/

	CCLabelTTF* playerScore = CCLabelTTF::create("", "Comic Sans MS", 19);
	playerScore->setAnchorPoint(ccp(0,1));
	playerScore->setPosition(ccp(startLeftPos, winSize.height - 10 - playerIndex*20));
	this->addChild(playerScore);
	playersScore[player->m_playerId] = playerScore;
		
	CCProgressTimer* hpProgress = CCProgressTimer::create(CCSprite::create("hp_progress.png"));
	if(hpProgress) {
		hpProgress->setAnchorPoint(ccp(0.5f,0.5f));
		hpProgress->setMidpoint(ccp(0, 0.5f));
		hpProgress->setType(kCCProgressTimerTypeBar);
		this->addChild(hpProgress);
		playersHp[player->m_playerId] = hpProgress;
	}

	CCProgressTimer* manaProgress = CCProgressTimer::create(CCSprite::create("mana_progress.png"));
	if(manaProgress) {
		manaProgress->setAnchorPoint(ccp(0.5f,0.5f));
		manaProgress->setMidpoint(ccp(0, 0.5f));
		manaProgress->setType(kCCProgressTimerTypeBar);
		this->addChild(manaProgress);
		playersMana[player->m_playerId] = manaProgress;
	}	
}

void Hud::updateHud()
{
	for(PlayerManager::TPlayerMap::iterator it = PlayerManager::instance().m_players.begin();
        it != PlayerManager::instance().m_players.end(); ++it)
    {
		Player* player = it->second;

		float offset = 10;

		CCProgressTimer* hp = playersHp[player->m_playerId];
		hp->setPosition(ccp(player->getPosition().x, player->getPosition().y + player->boundingBox().size.height*0.5f + offset));
		CCProgressTimer* mana = playersMana[player->m_playerId];
		mana->setPosition(ccp(hp->getPosition().x, hp->getPosition().y-6));
		CCLabelTTF* score = playersScore[player->m_playerId];

		hp->setPercentage(player->hp);
		mana->setPercentage(player->mana);
        
        if(player->isDead()) {
            score->setString(CCString::createWithFormat("%d Player score: %d (DEAD)", player->m_playerId, player->score)->getCString());
        } else {
            score->setString(CCString::createWithFormat("%d Player score: %d", player->m_playerId, player->score)->getCString());
        }
	}
}