//
//  PowerUp.cpp
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 07.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#include "StdAfx.h"
#include "PowerUp.h"

PowerUp::PowerUp()
{
    aready_used = false;
}

PowerUp::~PowerUp()
{
}

PowerUp * PowerUp::create(const std::string& imageName, void (* action)(Player *))
{
    PowerUp * powerUp = new PowerUp();
    powerUp->mAction = action;
    if(powerUp->initWithFile(imageName.c_str())) {
        
        CCFadeTo * fadeOut = CCFadeTo::create(50.0, 200);
        
        CCCallFuncN * destroyFunc = CCCallFuncN::create(powerUp, callfuncN_selector(PowerUp::onHide));
        
        powerUp->runAction(CCSequence::create(fadeOut, destroyFunc, NULL));
        
        powerUp->autorelease();
    } else {
        CC_SAFE_DELETE(powerUp);
    }
    return powerUp;
}

void PowerUp::onHide(CCNode * node)
{
    destroyPhysics();
    removeFromParent();
}

void PowerUp::collidedWithPlayer(Player * p)
{
    destroyPhysics();

    SimpleAudioEngine::sharedEngine()->playEffect("smb_powerup.wav");
    
    CCParticleExplosion * particle = CCParticleExplosion::create();
    particle->setAutoRemoveOnFinish(true);
    particle->setPosition(this->getPosition());
    
    getParent()->addChild(particle);
    
    removeFromParent();
    
    mAction(p);
}

void PowerUp::setupPhisics(b2World* world, float xPos, float yPos   )
{
	b2MassData mass = {100, b2Vec2(0,0), 10.0};
    
	physWorld = world;
	Box2DUtils::setupSprite(this, world, CCPointMake(xPos, yPos));
	getB2Body()->SetLinearDamping(0);
	getB2Body()->SetAngularDamping(0);
	getB2Body()->SetMassData(&mass);
	getB2Body()->SetUserData(this);
}

void PowerUp::destroyPhysics()
{
	Box2DUtils::destroySprite(this, physWorld);
}

