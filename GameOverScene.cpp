#include "StdAfx.h"
#include "GameOverScene.h"


GameOverScene::GameOverScene(void)
{
}


GameOverScene::~GameOverScene(void)
{
}

bool GameOverScene::init()
{
	CCLayer::init();
	return true;
}

CCScene* GameOverScene::scene()
{
	CCScene* scene = CCScene::create();
	scene->addChild(GameOverScene::create());
	return scene;
}
