//
//  Achievement.cpp
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 07.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#include "StdAfx.h"
#include "Achievement.h"
#include <algorithm>

Achievement::Achievement()
{
    
}

Achievement::~Achievement()
{
    
}

Achievement * Achievement::create(const std::string& name, int points, const std::string& image)
{
    Achievement * pRet = new Achievement();
    pRet->setupAchievement(name, points, image);
    if(pRet->init()) {
        pRet->autorelease();
    } else {
        CC_SAFE_DELETE(pRet);
    }
    return pRet;
}

void Achievement::setupAchievement(const std::string& name, int points, const std::string& image)
{
    this->name = name;
    this->points = points;
    this->image = image;
}

bool Achievement::init()
{
    if(!CCLayer::init())
        return false;
    
    setContentSize(CCSizeMake(120, 120));
    
    CCLabelTTF * label = CCLabelTTF::create(name.c_str(), "Comic Sans MS", 50);
    label->setColor(ccc3(255,0,0));
    
    this->addChild(label, 2);
    
    CCSprite * imageSprite = CCSprite::create(image.c_str());
    
    this->addChild(imageSprite, 2);
    
    CCFadeTo * fadeOut = CCFadeTo::create(2.0, 240);
    CCScaleBy * scaleBy = CCScaleBy::create(0.25, 3.0);
    
    CCCallFuncN * callFunc = CCCallFuncN::create(this, callfuncN_selector(Achievement::timedOut));
    
    runAction(CCSequence::create(fadeOut, scaleBy, callFunc, NULL));
    
    CCSize labelSize = label->getDimensions();
    CCSize spriteSize = imageSprite->getContentSize();
 
    CCSize totalSize = CCSizeMake(MAX(labelSize.width, spriteSize.width), spriteSize.height + 20 + labelSize.height);
    
    CCParticleExplosion * smoke = CCParticleExplosion::create();
    smoke->setStartColor(ccc4f(0.8,0.8,0.8,0.1));
    smoke->setPosition(totalSize.width / 2, totalSize.height / 2);
    
    addChild(smoke, 1);
    
    imageSprite->setPosition(CCPointMake((totalSize.width - spriteSize.width) / 2.0, 0));
    label->setPosition(CCPointMake((totalSize.width - labelSize.width) / 2.0, totalSize.height - labelSize.height));
    
    setContentSize(totalSize);
    
    return true;
}

void Achievement::timedOut(CCNode * node)
{
    node->removeFromParent();
}
