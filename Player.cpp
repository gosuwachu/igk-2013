#include "StdAfx.h"
#include "Player.h"
#include "Achievement.h"
#include "Map.h"

Player::Player(void)
{
	hp = 100.0f;
	mana = 100.0f;
	score = 0;
    hpLabel = NULL;
    world = NULL;

	motion1 = NULL;

    damageStarted = false;
    timeSinceLastDamage = 0;

    kills = 0;
    killsInRow = 0;

}

Player::~Player(void)
{
}

bool Player::initWithFile(const char *pszFilename)
{
	bool result = CCPhysicsSprite::initWithFile(pszFilename);

	this->scheduleUpdate();

	return result;
}

void Player::startMotionStreak()
{
	motion1 = CCMotionStreak::create(0.5f, 15, 45, ccWHITE, "trace.png");
	this->getParent()->addChild(motion1, 0);
}

void Player::update(float dt)
{
	if(motion1) {
		float angle = this->getRotation();
		motion1->setPosition( this->getPosition());
	}

    if(!damageStarted) {
        timeSinceLastDamage += dt;
    }
}

Player* Player::create()
{
	Player* pRet = new Player();
	if (pRet && pRet->initWithFile("tank.png"))
	{
		
		pRet->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(pRet);
	}
	return pRet;
}

void Player::updateJoyPosition(float dx, float dy)
{
    if(hp < 1)
        return;
    
    b2Vec2 localPosition = getB2Body()->GetLocalCenter();
    b2Vec2 localHead = b2Vec2(localPosition.x, localPosition.y + getContentSize().height / 10 / PTM_RATIO);
    b2Vec2 worldHead = getB2Body()->GetWorldPoint(localHead);

	float massFactor = getB2Body()->GetMass();
	b2Controller* controller;

	dx *= getVelocity();
	dy *= getVelocity();

	b2Vec2 v1 = getB2Body()->GetWorldVector(b2Vec2(0, 1));
	v1.Normalize();
	b2Vec2 v2 = b2Vec2(dx, -dy);
	v2.Normalize();

	float dir = b2Dot(v1, v2);
	if(dir > 0.1f && cur.touch) {
		float factor = getB2Body()->GetMass() * 80;
		// b2Vec2 v = getB2Body()->GetLinearVelocity();
		// v += b2Vec2(v1.x * dir * factor, v1.y * dir * factor);
		// getB2Body()->SetLinearVelocity(v);
		getB2Body()->ApplyForceToCenter( b2Vec2(v1.x * dir * factor, v1.y * dir * factor));
		// getB2Body()->ApplyLinearImpulse(b2Vec2(v1.x * dir * factor, v1.y * dir * factor), getB2Body()->GetWorldPoint(b2Vec2(0,0)));
	}

	if(cur.touch) {
		float atan = atan2(v2.y,v2.x) - atan2(v1.y,v1.x);

		while(atan < 0) {
			atan += 2 * M_PI;
		}

		atan -= M_PI;

		// CCLOG("atan: %f", atan);

		if(atan < -0.1f)
      getB2Body()->ApplyTorque(3600);
		else if(atan > 0.1f)
			getB2Body()->ApplyTorque(-3600);
	}

	// getB2Body()->ApplyLinearImpulse(b2Vec2(dx, -dy), worldHead);
}

void Player::setupPhysics( b2World* world )
{
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    Box2DUtils::setupSprite(this, world, CCPointZero);
    getB2Body()->SetAngularDamping(0.9);
    this->world = world;
}

void Player::fire(BulletType type)
{
    if(hp < 1)
        return;
    
	b2Vec2 localPosition = getB2Body()->GetLocalCenter();
    b2Vec2 localHead = b2Vec2(localPosition.x, localPosition.y + (1+getContentSize().height/2.0f) / PTM_RATIO);
    b2Vec2 worldHead = getB2Body()->GetWorldPoint(localHead);

	Bullet* bullet = Bullet::create(type);
	bullet->owner = this;
	bullet->setupPhisics(getB2Body()->GetWorld(), worldHead.x * PTM_RATIO, worldHead.y * PTM_RATIO);

  if(type == BT_Fire)
	  bullet->getB2Body()->ApplyForceToCenter(getB2Body()->GetWorldVector(b2Vec2(0,1000000)));
  else if(type == BT_Rocket)
    bullet->getB2Body()->ApplyForceToCenter(getB2Body()->GetWorldVector(b2Vec2(0,300000)));
  else {
	  b2MassData mass = {100000, b2Vec2(0,0), 1000.0f};
    bullet->getB2Body()->SetMassData(&mass);
    bullet->getB2Body()->ApplyForceToCenter(getB2Body()->GetWorldVector(b2Vec2(0,10000000)));
  }

  b2Vec2 xy = getB2Body()->GetWorldVector(b2Vec2(0,1));
  float realAngle = atan2f(xy.y, xy.x) - M_PI / 2 ;
  CCLOG("realAngle:%f", realAngle);

	this->getParent()->addChild(bullet);
	bullet->getB2Body()->SetTransform(worldHead, realAngle);
	bullet->startMotionStreak();
    
    SimpleAudioEngine::sharedEngine()->playEffect("fire.wav");
}

void Player::addAchievement(AchievementId a)
{
    if(achievements.count(a) > 0)
        return;
    
    achievements.insert(a);
    
    Achievement * obj = 0;
    
    switch (a) {
        case Got1000Points:
            obj = Achievement::create("100 Points !", 1000, "achiv.png");
            break;
        case FirstBlood:
            obj = Achievement::create("First Blood", 0, "skul.png");
            break;
        case DoubleKill:
            obj = Achievement::create("Double Kill", 0, "skul.png");
            break;
        case MultiKill:
            obj = Achievement::create("Multi Kill", 0, "skul.png");
            break;
        case SuicideKill:
            obj = Achievement::create("Burn in hell !!!", 0, "skul.png");
            break;
        case GotPotion:
            obj = Achievement::create("Heal !!!", 0, "achiv.png");
            break;
        case Got10000Points:
            obj = Achievement::create("1000 Points !", 0, "achiv.png");
            break;
        case WatchOutAchievement:
            obj = Achievement::create("Look out !!!", 0, "skul.png");
            break;
        default:
            break;
    }
    
    if(obj) {
        obj->setPosition(this->getPosition());
        
        getParent()->addChild(obj, 100);
    }
}

void Player::addScore(int theScore)
{
    int lastScore = this->score;
    
    this->score += theScore;
    
    if(lastScore < 100 && this->score >= 100) {
        this->addAchievement(Got1000Points);
    } else if(lastScore < 1000 && this->score >= 1000) {
        this->addAchievement(Got10000Points);
    }
}

void Player::playerKilledPlayer(Player * other)
{
    addScore(250);
    ++kills;
}

void Player::addHP(float hp)
{
    if(!canBeHurt())
        return;
    
//    damageStarted = true;
//    timeSinceLastDamage = 0;
    
    if(timeSinceLastDamage > 0.25) {
        timeSinceLastDamage = 0;
    }
    
    this->hp = max(min(this->hp + hp, 100.0f), 0.0f);
    
    if(hp != 0) {
        char hp_buffer[128];
        if(hp > 0) {
            sprintf(hp_buffer, "+%d HP", (int)hp);
        } else {
            sprintf(hp_buffer, "-%d HP", -(int)hp);
        }
        
        if(hpLabel) {
            hpLabel->setString(hp_buffer);
        } else {
            hpLabel = CCLabelTTF::create(hp_buffer, "Comic Sans MS", 50);
            
            getParent()->addChild(hpLabel, 99);
            
            if(hp < 0) {
                hpLabel->setColor(ccc3(255, 0, 0));
            } else {
                hpLabel->setColor(ccc3(0, 255, 0));
            }
            
            hpLabel->runAction(CCSequence::create(
                                                CCMoveBy::create(0.5, CCPointMake(2.0, 15.0)),
                                                CCFadeOut::create(0.5),

                                                CCCallFuncN::create(this, callfuncN_selector(Player::destroyHp)), 
                                                NULL));
        }
        
        CCPoint labelPos = CCPointMake(getContentSize().width / 2.0, getContentSize().height + 10);
        
        labelPos.x += this->getPosition().x;
        labelPos.y += this->getPosition().y;
        
        hpLabel->setPosition(labelPos);
    }
 
    if(this->isDead() && !this->getChildByTag(0x2001)) {
        CCSprite * trollface = CCSprite::create("trollface.png");
        
//        trollface->setScale(0.21);
        trollface->setTag(0x2001);
        
        trollface->setPosition(CCPointMake(getContentSize().width / 2, getContentSize().height / 2));
        
        addChild(trollface);

        CCDirector::sharedDirector()->getScheduler()->scheduleSelector(
            schedule_selector(Player::spawnPlayer), this,
            0, 0, 2, false);

		// TODO: Stop physics for this player or remove him

    } else if(!this->isDead()) {
        CCNode * node = this->getChildByTag(0x2001);
        if(node != NULL) {
            node->removeFromParent();
        }
    }
}

void Player::spawnPlayer(float dt)
{
	Box2DUtils::destroySprite(this, world);
    reset();
    Map * map = (Map *)getParent();
    
    setupPhysics(world);
    
	getB2Body()->SetAngularDamping(10);
	getB2Body()->SetLinearDamping(10);

	b2MassData mass = {100, b2Vec2(0,0), 100.0f};
	getB2Body()->SetMassData(&mass);

    // set spawn point
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    setPosition(map->spawPoints[rand() % map->spawPoints.size()]);
  
	  startMotionStreak();
    
	  getB2Body()->SetAngularDamping(10);
	  getB2Body()->SetLinearDamping(10);
}

void Player::destroyHp(CCNode *node)
{
    if(node) {
        node->removeFromParent();
        node = NULL;
    }
	hpLabel = NULL;
}

float Player::getVelocity() const
{
//    if(hp >= 80)
        return 30.0;
    
//    return 30.0 * (this->hp / 100.0);
}

bool Player::canBeHurt()
{
    if(isDead())
        return false;
    
    if(timeSinceLastDamage > 0.1) {
        damageStarted = false;
    }
    
    return timeSinceLastDamage > 0.1;
}

void Player::reset()
{
    hp = 100;
    mana = 100;
    killsInRow = 0;
    addHP(0);
    getB2Body()->SetLinearVelocity(b2Vec2(0,0));
    getB2Body()->SetAngularVelocity(0.0f);
}
\
void Player::postPhysicsUpdate()
{
    if(isDead()) {
        if(this->getB2Body()->GetType() != b2_staticBody) {
            getB2Body()->SetType(b2_staticBody);
            getB2Body()->SetTransform(getB2Body()->GetPosition(), 0);
        }
    } else {
        if(this->getB2Body()->GetType() == b2_staticBody) {
            getB2Body()->SetType(b2_dynamicBody);
        }
    }
}\
