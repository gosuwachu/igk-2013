﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Diagnostics;
using System.Net.Sockets;
using Microsoft.Phone.Net.NetworkInformation;
using Microsoft.Devices.Sensors;
using System.IO;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;

namespace TankController
{
    public partial class MainPage : PhoneApplicationPage
    {
        private Socket socket;
        private Timer timer;
        private Accelerometer accelerometer;

        public static IPAddress address = IPAddress.Parse("192.168.137.1");

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            ResetPosition(rootCircle, centerCircle);

            accelerometer = new Accelerometer();
            timer = new Timer(TimerCallback, null, 100, 100);

            Touch.FrameReported += Touch_FrameReported;
        }

        void SimulateClick(System.Windows.Controls.Primitives.ToggleButton b, TouchFrameEventArgs e)
        {
            bool down = false;
            int pointsNumber = e.GetTouchPoints(b).Count;
            TouchPointCollection pointCollection = e.GetTouchPoints(b);
            for (int i = 0; i < pointsNumber; i++)
            {
                double x = pointCollection[i].Position.X;
                double y = pointCollection[i].Position.Y;
                if (pointCollection[i].Action == TouchAction.Down ||
                    pointCollection[i].Action == TouchAction.Move)
                {
                    if (x < 0 || y < 0 || x > b.Width || y > b.Height)
                        continue;
                    down = true;
                }
            }
            if (b.IsChecked.GetValueOrDefault() != down)
                b.IsChecked = down;
        }

        void TouchPanel(Ellipse root, Ellipse center, TouchFrameEventArgs e)
        {
            int touchRegion = 0;
            if (touched)
                touchRegion = 200;
            touched = false;
            int pointsNumber = e.GetTouchPoints(root).Count;
            TouchPointCollection pointCollection = e.GetTouchPoints(root);
            for (int i = 0; i < pointsNumber; i++)
            {
                double x = pointCollection[i].Position.X;
                double y = pointCollection[i].Position.Y;
                if (pointCollection[i].Action == TouchAction.Down ||
                    pointCollection[i].Action == TouchAction.Move)
                {
                    if (x < -touchRegion || y < -touchRegion || x > root.Width + touchRegion || y > root.Height + touchRegion)
                        continue;
                    SetPosition(root, center, x, y);
                    touched = true;
                }
            }

            if(!touched)
            {
                ResetPosition(root, center);
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            SimulateClick(button1, e);
            SimulateClick(button2, e);
            SimulateClick(button3, e);
            SimulateClick(button4, e);
            TouchPanel(rootCircle, centerCircle, e);
        }

        private void TimerCallback(object state)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() => SendState());
        }

        private void SendState()
        {
            if (!DeviceNetworkInformation.IsNetworkAvailable)
                return;

            if (socket == null)
            {
                NetworkInterfaceInfo networkInterface = null;

                foreach (NetworkInterfaceInfo info in new NetworkInterfaceList())
                {
                    if (info.InterfaceType == NetworkInterfaceType.Wireless80211)
                    {
                        networkInterface = info;
                        break;
                    }
                }

                if (networkInterface == null)
                {
                    Debug.WriteLine("No interface");
                    return;
                }

                Debug.WriteLine("Connecting...");
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                // socket.SetNetworkRequirement(NetworkSelectionCharacteristics.NonCellular);
                // socket.SetNetworkPreference(NetworkSelectionCharacteristics.NonCellular);
                // socket.SetNetworkRequirement(NetworkSelectionCharacteristics.NonCellular);
                // socket.AssociateToNetworkInterface(networkInterface);
                SocketAsyncEventArgs connectEvent = new SocketAsyncEventArgs();
                connectEvent.Completed += connectEvent_Completed;
                connectEvent.RemoteEndPoint = new IPEndPoint(address, 12900);
                socket.ConnectAsync(connectEvent);
                return;
            }
            else if (!socket.Connected)
            {
                return;
            }

            byte[] tmp;

            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    Vector2 xy = GetNormalizedPosition(rootCircle, centerCircle);
                    Vector3 acc = new Vector3();

                    if (accelerometer.IsDataValid && accelerometer.State == SensorState.Ready)
                    {
                        acc = accelerometer.CurrentValue.Acceleration;
                    }

                    int player = 0;

                    if (p1.IsChecked.GetValueOrDefault())
                        player = 1;
                    else if (p2.IsChecked.GetValueOrDefault())
                        player = 2;
                    else if (p3.IsChecked.GetValueOrDefault())
                        player = 3;
                    else if (p4.IsChecked.GetValueOrDefault())
                        player = 4;
                    else
                        return;

                    Debug.WriteLine("P:{7}, X:{0} Y:{1} T:{2} AX:{8} AY:{9} AZ:{10} B:{3}{4}{5}{6}", xy.X, xy.Y, touched,
                        button1.IsChecked.GetValueOrDefault() ? 1 : 0,
                        button2.IsChecked.GetValueOrDefault() ? 1 : 0,
                        button3.IsChecked.GetValueOrDefault() ? 1 : 0,
                        button4.IsChecked.GetValueOrDefault() ? 1 : 0,
                        player,
                        acc.X, acc.Y, acc.Z);

                    writer.Write((int)player); // player
                    writer.Write((float)xy.X); // x 
                    writer.Write((float)xy.Y); // y
                    writer.Write(touched); // touch
                    writer.Write(button1.IsChecked.GetValueOrDefault() ? 1 : 0);
                    writer.Write(button2.IsChecked.GetValueOrDefault() ? 1 : 0);
                    writer.Write(button3.IsChecked.GetValueOrDefault() ? 1 : 0);
                    writer.Write(button4.IsChecked.GetValueOrDefault() ? 1 : 0);
                    writer.Write((float)acc.X); // ax 
                    writer.Write((float)acc.Y); // ay
                    writer.Write((float)acc.Z); // az
                }

                tmp = stream.ToArray();
            }

            SocketAsyncEventArgs sendEvent = new SocketAsyncEventArgs();
            sendEvent.SetBuffer(tmp, 0, tmp.Length);
            sendEvent.RemoteEndPoint = new IPEndPoint(address, 12900);
            sendEvent.Completed += sendEvent_Completed;
            if(socket != null)
                socket.SendToAsync(sendEvent);
        }

        void sendEvent_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
                socket = null;
        }

        void connectEvent_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
                socket = null;
        }

        Vector2 GetNormalizedPosition(Ellipse root, Ellipse center)
        {
            double cx = root.Width / 2;
            double cy = root.Height / 2;
            double x = center.Margin.Left + center.Width / 2;
            double y = center.Margin.Top + center.Height / 2;
            x -= cx;
            y -= cy;
            x -= root.Margin.Left;
            y -= root.Margin.Top;
            x /= cx;
            y /= cy;
            return new Vector2((float)x, (float)y);
        }

        void ResetPosition(Ellipse root, Ellipse center)
        {
            SetPosition(root, center, root.Width / 2, root.Height / 2);
        }

        void SetPosition(Ellipse root, Ellipse center, double x, double y)
        {
            double cx = root.Width / 2;
            double cy = root.Height / 2;
            double maxR = Math.Max(cx, cy);
            x -= cx;
            y -= cy;
            double R = Math.Sqrt(x * x + y * y);
            if (R > maxR)
            {
                x /= R / maxR;
                y /= R / maxR;
            }
            cx += root.Margin.Left;
            cy += root.Margin.Top;
            Thickness thick = new Thickness(cx + x - center.Width / 2,
                cy + y - center.Height / 2,
                cx + x + center.Width / 2,
                cy + y + center.Height / 2);
            center.Margin = thick;
        }

        bool touched;

        void EnablePlayer(System.Windows.Controls.Primitives.ToggleButton button)
        {
            bool isChecked = button.IsChecked.GetValueOrDefault();
            
            if (p1 != button)
                p1.IsChecked = false;
            if (p2 != button)
                p2.IsChecked = false;
            if (p3 != button)
                p3.IsChecked = false;
            if (p4 != button)
                p4.IsChecked = false;

            if (isChecked)
            {
                accelerometer.Start();
            }
            else
            {
                accelerometer.Stop();
                if (socket != null)
                {
                    socket.Dispose();
                    socket = null;
                }
            }
        }

        private void p1_Checked(object sender, RoutedEventArgs e)
        {
            EnablePlayer(p1);
        }

        private void p2_Checked(object sender, RoutedEventArgs e)
        {
            EnablePlayer(p2);
        }

        private void p3_Checked(object sender, RoutedEventArgs e)
        {
            EnablePlayer(p3);
        }

        private void p4_Checked(object sender, RoutedEventArgs e)
        {
            EnablePlayer(p4);
        }

        private void EndIdentity(IAsyncResult result)
        {
            string newIdentity = Guide.EndShowKeyboardInput(result);

            address = IPAddress.Parse(newIdentity);

            if (socket != null)
            {
                socket.Dispose();
                socket = null;
            }
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {
            Guide.BeginShowKeyboardInput(PlayerIndex.One,
                "controller", "Enter IP address:", address.ToString(), this.EndIdentity, null);
        }
    }
}