#include "StdAfx.h"
#include "GameBlock.h"

int threshold = 3;
int howManyObjectsToSpawn = 4;

Block::Block()
{
	blockType = BT_Wood;
	posX = 0;
	posY = 0;
	currentDestructionIndex = 0;
	hp = 100;
	destroyed = false;
}

Block::~Block()
{
	//this->removeFromParent();
}

Block* Block::create(BlockType type, float x, float y, int currentDestructionIndex)
{
	Block* pRet = new Block();
	pRet->setupBlock(type, x, y, currentDestructionIndex);
	if (pRet && pRet->initWithFile(pRet->fileName.c_str()))
	{
		pRet->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(pRet);
	}
	return pRet;
}

void Block::setupBlock(BlockType type, float x, float y, int idx)
{
	blockType = type;
	posX = x;
	posY = y;
	currentDestructionIndex = idx;

	setupGfx();
}

void Block::setupGfx()
{
	std::stringstream ss;
	switch(blockType)
	{
	case BT_Wood:
		ss << "stone";
		break;
	case BT_Brick:
		ss << "brick";
		break;
	case BT_Stone:
		fileName = "stone.png";
		return;
    case BT_Plant:
        fileName = "plant.png";
        return;
	}

	ss << "-" << currentDestructionIndex << ".jpg";
	fileName = ss.str();
}

void Block::destroy()
{
    if(blockType == BT_Stone)
        return; 
    if(blockType == BT_Plant)
        return;

    float length = getB2Body()->GetLinearVelocity().Length();

    int index = currentDestructionIndex+1;

    if(length > 4)
      index++;
    if(length > 7)
      index++;

    // CCLOG("idx:%i, velocity: %f", index, getB2Body()->GetLinearVelocity().Length());

	if(index >= threshold)
	{
		//this->removeFromParent();
	} else 
	{
		for(int i = 0; i < howManyObjectsToSpawn; ++i)
		{
			float singleWidth = this->boundingBox().size.width/(howManyObjectsToSpawn*0.5f);
			float singleHeight = this->boundingBox().size.height/(howManyObjectsToSpawn*0.5f);
			CCPoint centerPosParent = this->getPosition();
			CCPoint leftTop = ccp(centerPosParent.x-boundingBox().size.width*0.5f, centerPosParent.y+boundingBox().size.height*0.5f);
			
			int row = i/(howManyObjectsToSpawn*0.5f);
			int col = i-row*(howManyObjectsToSpawn*0.5f);

			CCPoint childCenter = ccp(leftTop.x + singleWidth + singleWidth*col, leftTop.y - singleHeight + singleHeight*row);

			Block* child = Block::create(blockType, childCenter.x, childCenter.y, index);
			child->setTag(5);
			child->setScaleX(this->getScaleX());
			child->setScaleY(this->getScaleY());
			child->setupPhisics(physWorld);
			this->getParent()->addChild(child);

			if(index+1 == threshold) {
				child->getB2Body()->SetActive(false);
			}
			else {
				child->getB2Body()->SetLinearVelocity(getB2Body()->GetLinearVelocity());
				child->getB2Body()->SetAngularVelocity(getB2Body()->GetAngularVelocity());
			}
		}
	}
	this->spawnExplosion();
	this->destroyPhysics();
	this->removeFromParent();
}

void Block::spawnExplosion()
{
	CCString* fileName = CCString::createWithFormat("ExplodingRing%d.plist", currentDestructionIndex+1);
	CCParticleSystem* fire = CCParticleSystemQuad::create(fileName->getCString());
	fire->setPosition(this->getPosition());
	fire->setAutoRemoveOnFinish(true);
	fire->setScale(0.2f);
	this->getParent()->addChild(fire, 1);
}

void Block::setupPhisics(b2World* world)
{
	b2MassData mass = {50, b2Vec2(0,0), 1};

	physWorld = world;

	//Box2DUtils::setupSprite(this, world, ccp(posX, posY));
    CCPoint p = ccp(posX, posY);

    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);

    b2Body *body = world->CreateBody(&bodyDef);

    // Define another box shape for our dynamic body.
    b2PolygonShape dynamicBox;
    if(blockType == BT_Plant)
    {
        dynamicBox.SetAsBox((this->getContentSize().width * 0.5 * this->getScaleX()) / 2 / PTM_RATIO, 
            (this->getContentSize().height * 0.5 * this->getScaleY()) / 2 / PTM_RATIO);
    }
    else
    {
        dynamicBox.SetAsBox((this->getContentSize().width * this->getScaleX()) / 2 / PTM_RATIO, 
            (this->getContentSize().height * this->getScaleY()) / 2 / PTM_RATIO);
    }

	if(blockType == BT_Stone)
	{
		body->SetType(b2_staticBody);
	}

	if(blockType == BT_Plant)
	{
		body->SetActive(false);
	}

    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &dynamicBox;    
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.3f;
    body->CreateFixture(&fixtureDef);    

    this->setB2Body(body);
    this->setPTMRatio(PTM_RATIO);
    this->setPosition( ccp( p.x, p.y) );

	getB2Body()->SetLinearDamping(10.0);
	getB2Body()->SetAngularDamping(10.0);
	getB2Body()->SetMassData(&mass);
	getB2Body()->SetUserData(this);
}

void Block::destroyPhysics()
{
	Box2DUtils::destroySprite(this, physWorld);
}

