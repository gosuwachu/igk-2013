#pragma once
#include "includes.h"

#ifdef _WIN32
class IGKEAGLView : public CCEGLView
{
public:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	static IGKEAGLView* sharedOpenGLView();
};
#endif
