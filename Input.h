#pragma once
#include "includes.h"
//#include "Common.h"

class Input
{
public:
	Input()				{}
	virtual ~Input()	{}

	static Input* instance();


#ifdef _WIN32
	void init(HINSTANCE appInstance, bool isExclusive = true);
#endif
	void release();

	bool keyDown(int key)    { return  keys[key];  }
	bool keyUp(int key)      { return (!keys[key] && tempKeys[key]); }
	bool keyPressed(int key) { return (keys[key]  && tempKeys[key]); }

	char getChar() 
	{
		for (int i = ' '; i < '~'; i++) 
		{
			if (keyPressed(i))
				return (char)i;
		}
		return '\0';
	}

	void update();
    
#ifdef __APPLE__
    void setKey(int key, bool down);
#endif

private:
#ifdef __APPLE__
    bool keys[0x10000];
	bool tempKeys[0x10000];
#else
	bool keys[256];
	bool tempKeys[256];
#endif
};