#pragma once
#include "includes.h"
#include "GameBlock.h"

class Map : public CCLayer
{
public:
    Map(int levelNumber, b2World* world);
    ~Map(void);

    void loadMap(const std::string& name);

    static Map* create(int levelNumber, b2World* world);

    virtual bool init();

    void setBlockAt(float x, float y, float blockWidth, float blockHeight, int type);
    void defineStaticWalls();
public:
    int levelNumber;
    int width;
    int height;
    CCPoint origin;
    b2World* world;
    float mapWidth;
    float mapHeight;
	std::vector<Block*> blockChilds;

    CCSprite* background;

    std::vector<CCPoint> spawPoints;
};

