//
//  Achievement.h
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 07.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#ifndef __IGK2013__Achievement__
#define __IGK2013__Achievement__

#include "includes.h"

class Achievement : public CCLayer
{
public:
    Achievement();
    ~Achievement();
    
    static Achievement * create(const std::string& name, int points, const std::string& image);
    
    void setupAchievement(const std::string& name, int points, const std::string& image);
    
    virtual bool init();
    
    void timedOut(CCNode * node);
    
private:
    std::string name;
    int points;
    std::string image;
};

#endif /* defined(__IGK2013__Achievement__) */
