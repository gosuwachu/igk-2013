#pragma once
#include "includes.h"
#include "Bullet.h"

#include "NetworkThread.h"
#include <set>

enum AchievementId {
    Got1000Points,
    FirstBlood,
    DoubleKill,
    MultiKill,
    SuicideKill,
    GotPotion,
    Got10000Points,
    WatchOutAchievement
};

class Player : public CCPhysicsSprite
{
public:
    unsigned int m_playerId;

    NetworkPacket cur;
    NetworkPacket prev;
    
    
	Player(void);
	~Player(void);

	static Player* create();

	virtual bool initWithFile(const char *pszFilename);

	void update(float dt);

  b2World* world;
    
    void updateJoyPosition(float dx, float dy);
    void setupPhysics( b2World* world );
	void fire(BulletType type);

    void addAchievement(AchievementId a);
    
    void addScore(int scrore);
    void addHP(float hp);
    void playerKilledPlayer(Player * other);
    
    bool canBeHurt();

    void reset();
    
    bool isDead() const {
        return hp < 1;
    }

    void spawnPlayer(float dt);
    
    void applyDamage(float hp);
    
    float getVelocity() const;
    
    void destroyHp(CCNode *);

	void startMotionStreak();
    
    void postPhysicsUpdate();
    
public:
    
    
	float hp;
	float mana;
	int score;
    int kills;
    int killsInRow;
    
    std::set<AchievementId> achievements;
    CCLabelTTF * hpLabel;

	CCMotionStreak* motion1;

    bool damageStarted;
    float timeSinceLastDamage;
};

