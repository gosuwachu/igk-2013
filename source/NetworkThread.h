//
//  NetworkThread.h
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 06.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#ifndef __IGK2013__NetworkThread__
#define __IGK2013__NetworkThread__

#include <pthread.h>
#include <errno.h>
#if defined(__WIN32__) || defined(_WIN32)
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#endif

#include <deque>
#include <vector>

#pragma pack(push, 1)
struct NetworkPacket
{
    int player;
    float x, y;
    bool touch;
    int b1, b2, b3, b4;
    float ax, ay, az;
};
#pragma pack(pop)

class NetworkThread
{
public:
    static NetworkThread& instance();
    
    std::vector<NetworkPacket> getPackets();
    void pushPacket(NetworkPacket p);
    
private:
    NetworkThread();
    ~NetworkThread();
    
private:
    void start();
    
private:
    pthread_t thread_ref;
    
    static void * thread_entry(void *);
    
private:
    pthread_mutex_t queue_lock;
    std::deque<NetworkPacket> packetQueue;
};

#endif /* defined(__IGK2013__NetworkThread__) */
