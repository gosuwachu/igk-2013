#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "includes.h"
#include "Player.h"
#include "Map.h"
#include "NetworkThread.h"
#include "Hud.h"
#include "PowerUp.h"

class ContactListener;

class GameScene : public CCLayerColor
{
public:
    virtual bool init(int level);  

    ~GameScene();

	void initPlayer( CCSize &visibleSize, CCPoint &origin );

    static CCScene* scene(int level);
    
    // implement the "static node()" method manually
    //CREATE_FUNC(GameScene);
	static GameScene* create(int level);

	virtual void ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent );

    void destroyBlockAtPosition( CCPoint position );

	virtual void ccTouchesMoved( CCSet *pTouches, CCEvent *pEvent );

	virtual void ccTouchesEnded( CCSet *pTouches, CCEvent *pEvent );

	virtual void onEnter();

	void playBackgroundMusic();

	virtual void onExit();

	void stopBackgroundMusic();

	void initPhysics();

	void initWorld();
    
    void killWorld();

	void initBox2DDebugDraw();

    void initMap(int levelNUmber);

	void initHud();
    
    void initItemSpawner();

	virtual void draw();

	void DrawBox2DDebugData();

	virtual void update(float delta);
    
    void updateNetwork(float delta);

    Player * createPlayer(const NetworkPacket &packet );

    void proccessPacket(NetworkPacket packet);

    void updatePowerUps(float dt);

	void checkKeyboardKeys();

	Bullet* findLatestBullet(Player* owner, BulletType type);
    
    void playerKilledPlayer(Player * killer, Player * killed);
    
private:
	b2World* world;
	Player* player;
    Map* map;
	Hud* hud;

    CCSprite* tutorial;

public:
    CCTimer * spawnTimer;
	float timeScale;
	float expectedTimeScale;
    int totalKills;
    Player * lastKiller;

    ContactListener* contactListener;
};

#endif // __HELLOWORLD_SCENE_H__
