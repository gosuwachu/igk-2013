#include "StdAfx.h"
#include "GameScene.h"
#include "GLES-Render.h"
#include "PlayerManager.h"
#include "GameBlock.h"
#include "NetworkThread.h"
#include "Achievement.h"
#include "PowerUp.h"
#include "Input.h"

CCScene* GameScene::scene(int level)
{
    CCScene *scene = CCScene::create();
    GameScene *layer = GameScene::create(level);
    scene->addChild(layer);
    return scene;
}

GameScene* GameScene::create(int level)
{
	GameScene* pRet = new GameScene();
    if (pRet && pRet->init(level))
    {
        pRet->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRet);
    }
    return pRet;
}

GameScene::~GameScene()
{
  /*
  PlayerManager::instance().resetPlayers();
  delete world;
  delete map;
  delete hud;
  delete tutorial;
  delete spawnTimer;*/
}

// on "init" you need to initialize your instance
bool GameScene::init(int level)
{
  spawnTimer = NULL;

    CCLayerColor::init();
	scheduleUpdate();
	setTouchEnabled(true);
    
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    PlayerManager::instance().resetPlayers();
    
	initPhysics();
	initBox2DDebugDraw();
	//initPlayer(visibleSize, origin);
    initMap(level);
	initHud();
    initItemSpawner();
	timeScale = 1.0f;
	expectedTimeScale = 1.0f;

    totalKills = 0;
    lastKiller = NULL;    
    tutorial = NULL;
    
    return true;
}

void GameScene::ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent )
{
#ifdef _WIN32
	if(~GetKeyState(VK_CAPITAL) & 0x0001)
		return;
#endif
	CCTouch* touch = (CCTouch*)pTouches->anyObject();
	if(touch)
	{
        destroyBlockAtPosition(touch->getLocation());

        kmVec2 v;
        v.x = touch->getLocation().x - getPositionX();
        v.y = touch->getLocation().y - getPositionY();
        kmVec2Normalize(&v, &v);

        NetworkPacket p;
        p.player = 0;
        p.x = v.x;
        p.y = v.y;
        NetworkThread::instance().pushPacket(p);
 	}
}

void GameScene::ccTouchesMoved( CCSet *pTouches, CCEvent *pEvent )
{
#ifdef _WIN32
	if(~GetKeyState(VK_CAPITAL) & 0x0001)
		return;
#endif
    CCTouch* touch = (CCTouch*)pTouches->anyObject();
    if(touch)
    {

        kmVec2 v;
        v.x = touch->getLocation().x - getContentSize().width / 2;
        v.y = touch->getLocation().y - getContentSize().height / 2;

        NetworkPacket p;
        p.player = 0;
        p.x = v.x;
        p.y = v.y;
        NetworkThread::instance().pushPacket(p);
    }
}

void GameScene::ccTouchesEnded( CCSet *pTouches, CCEvent *pEvent )
{
}

void GameScene::onEnter()
{
	CCLayer::onEnter();
	// TODO:
	playBackgroundMusic();
}

void GameScene::onExit()
{
	stopBackgroundMusic();
	CCLayer::onExit();
}

void GameScene::initPhysics()
{
	initWorld();
}

class ContactListener : public b2ContactListener
{
    std::list< std::pair<Player *, PowerUp *> > powerUpCollisions;
    GameScene * scene;
    
    
    virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
    {
        if(!scene)
            return;
        
		const b2Fixture* a = contact->GetFixtureA();
		const b2Fixture* b = contact->GetFixtureB();
		const b2Body* aa = a->GetBody();
		const b2Body* bb = b->GetBody();
		CCPhysicsSprite* as = (CCPhysicsSprite*)aa->GetUserData();
		CCPhysicsSprite* bs = (CCPhysicsSprite*)bb->GetUserData();
        
        if(Player * player = dynamic_cast<Player *>(as)) {
            testPlayerCollision(player, bs);
        } else if(Player * player = dynamic_cast<Player *>(bs)) {
            testPlayerCollision(player, as);
        } else if(Block * block = dynamic_cast<Block *>(as)) {
            testBlockCollision(block, bs);
        } else if(Block * block = dynamic_cast<Block *>(bs)) {
            testBlockCollision(block, as);
        } else {
			destroyObject(as);
			destroyObject(bs);
		}
    }
    
    void testBlockCollision(Block * block, CCPhysicsSprite * other) {
        if(Block * otherBlock = dynamic_cast<Block *>(other)) {
            if(block->currentDestructionIndex < otherBlock->currentDestructionIndex) {
                destroyBlock(otherBlock);
            } else if(block->currentDestructionIndex > otherBlock->currentDestructionIndex) {
                destroyBlock(block);
            } else {
                destroyBlock(block);
                destroyBlock(otherBlock);
            }
        } else {
            destroyBlock(block);
            destroyObject(other);
        }
    }
    
    void testPlayerCollision(Player * player, CCPhysicsSprite * other) {
        if(player->isDead())
            return;
        
       if(PowerUp * pup = dynamic_cast<PowerUp *>(other)) {
            if(!pup->aready_used) {
                pup->aready_used = true;
                player->retain();
                pup->retain();
                powerUpCollisions.push_back(std::make_pair(player, pup));
            }
        } else if(Block * block = dynamic_cast<Block *>(other)) {
            
             
          if(block->getB2Body()->GetLinearVelocity().Length() < 0.05f)
            return;
          if(block->currentDestructionIndex == 2)
            return;
            
            player->addScore(1);
            
          //CCLOG("velocity len: %f", block->getB2Body()->GetLinearVelocity().Length());

            if(player->canBeHurt())
            {
                if(block->blockType == BT_Plant)
                    SimpleAudioEngine::sharedEngine()->playEffect("pick.wav");
                else
                    SimpleAudioEngine::sharedEngine()->playEffect("wall.wav");
            }

            destroyBlock(block);

            if(block->currentDestructionIndex == 0)
            {
              player->addHP((block->blockType + 1) * -1.0);
            
              if(player->isDead()) {
                  player->addAchievement(SuicideKill);
              }
            }
        } else if(Bullet * bullet = dynamic_cast<Bullet*>(other)) {
			if(bullet->owner == player)
				return;
            
            player->addHP(-bullet->getDamage());
            
            if(player->isDead()) {
                // Kills
                if(!bullet->owner->isDead()) {
                    bullet->owner->playerKilledPlayer(player);
                    bullet->owner->addScore(-100);
                }
                
                scene->playerKilledPlayer(bullet->owner, player);
            }
            
			destroyBullet(bullet);
		}
    }

	void destroyObject(CCPhysicsSprite* s)
	{
		if(!s)
			return;

		if(Block* b = dynamic_cast<Block*>(s)) {
			destroyBlock(b);
        } else if(Bullet * bullet = dynamic_cast<Bullet*>(s)) {
			destroyBullet(bullet);
		}
	}

	void destroyBlock(Block* b)
	{
		/*
		CCLOG("Block about to be destroyed: length:%f",
			b->getB2Body()->GetLinearVelocity().Length() 
			);*/

		if(b->getB2Body()->GetLinearVelocity().Length() > 1)
			b->destroyed = true;
	}

public:
    ContactListener() {
        scene = NULL;
    }
    
    void setScene(GameScene * s)
    {
        scene = s;
        powerUpCollisions.clear();
    }
    
    void invokePowerUps()
    {
        std::list< std::pair<Player *, PowerUp *> >  cols;
        cols.swap(powerUpCollisions);
        
        for(std::list< std::pair<Player *, PowerUp *> >::iterator it = cols.begin(); it != cols.end() ; ++it) {
            Player * player = it->first;
            PowerUp * pup = it->second;
            
            if(!player->isDead()) {
                pup->collidedWithPlayer(player);
            } else {
                pup->removeFromParent();
            }
            
            player->release();
            pup->release();
        }
    }


	void destroyBullet(Bullet* b)
	{
		b->destroyed = true;

	  CCString* fileName = CCString::createWithFormat("ExplodingRing.plist");
	  CCParticleSystem* fire = CCParticleSystemQuad::create(fileName->getCString());
	  fire->setPosition(b->getPosition());
	  fire->setAutoRemoveOnFinish(true);
	  fire->setScale(0.5f);
	  b->getParent()->addChild(fire, 1);
	}
};

void GameScene::initWorld()
{
	b2Vec2 gravity;
	gravity.Set(0.0f, 0.0f);
	world = new b2World(gravity);

    contactListener = new ContactListener();

	// Do we want to let bodies sleep?
	world->SetAllowSleeping(true);
	world->SetContinuousPhysics(true);
	world->SetContactListener(contactListener);
    contactListener->setScene(this);
}

void GameScene::killWorld()
{
    contactListener->setScene(NULL);
}

void GameScene::initPlayer( CCSize &visibleSize, CCPoint &origin )
{
	player = Player::create();
	player->setAnchorPoint(ccp(0.5f, 0.5f));
	Box2DUtils::setupSprite(player, world, ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	this->addChild(player);
	player->startMotionStreak();
}



void GameScene::playBackgroundMusic()
{
	SimpleAudioEngine::sharedEngine()->playBackgroundMusic("background.mp3", true);
    SimpleAudioEngine::sharedEngine()->setEffectsVolume(0.7);
}

void GameScene::stopBackgroundMusic()
{
	SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
}

void GameScene::draw()
{
	CCLayerColor::draw();

	DrawBox2DDebugData();
}

class MyQueryCallback : public b2QueryCallback
{
public:
  std::vector<b2Body*> foundBodies;

  bool ReportFixture(b2Fixture* fixture) {
      foundBodies.push_back( fixture->GetBody() );
      return true;//keep going to find all fixtures in the query area
  }

  void applyBlastImpulse(b2Body* body, b2Vec2 blastCenter, b2Vec2 applyPoint, float blastPower) {
      b2Vec2 blastDir = applyPoint - blastCenter;
      float distance = blastDir.Normalize();
      //ignore bodies exactly at the blast point - blast direction is undefined
      if ( distance == 0 )
          return;
      float invDistance = 1 / distance;
      float impulseMag = blastPower * invDistance * invDistance;
      body->ApplyLinearImpulse( impulseMag * blastDir, applyPoint );
  }
};


void GameScene::update(float delta)
{
  if(!world)
    return;

	float tau = 0.5f;
	timeScale = timeScale + (expectedTimeScale - timeScale) * (1.0f - exp(-delta/0.01f));
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(expectedTimeScale);

	// CCLOG("timeScale:%f expected:%f", timeScale, expectedTimeScale);

    updateNetwork(delta);
	CCLayerColor::update(delta);
	hud->updateHud();
	Input::instance()->update();
	this->checkKeyboardKeys();
	world->Step(delta, 1, 1);

	b2Body* first = world->GetBodyList();
	while(first)
	{
		b2Body* current = first;
		first = first->GetNext();

		CCPhysicsSprite* sprite = (CCPhysicsSprite*)current->GetUserData();
		if(!sprite)
			continue;

		if(Block * b = dynamic_cast<Block*>(sprite)) {
			if(b->destroyed) {
				b->destroy();
			}
		}

		else if(Bullet * b = dynamic_cast<Bullet*>(sprite)) {
			if(b->destroyed) {
                SimpleAudioEngine::sharedEngine()->playEffect("explosion.wav");

        float blastRadius = 0.0f;
        float blastPower = 0.0f;
        float scale = 0.0f;

        if(b->bulletType  == BT_Bomb) {
          blastRadius = 150.0f/32.0f;
          blastPower = 3000.0f;
          scale = 50.0f;
        }
        else if(b->bulletType == BT_Rocket) {
          blastRadius = 60.0f/32.0f;
          blastPower = 400.0f;
          scale = 3.0f;
        }

        if(blastRadius > 0.0f) {
          b2Vec2 center = b->getB2Body()->GetWorldCenter();
           //find all bodies with fixtures in blast radius AABB
          MyQueryCallback queryCallback; //see "World querying topic"
          b2AABB aabb;
          aabb.lowerBound = center - b2Vec2( blastRadius, blastRadius );
          aabb.upperBound = center + b2Vec2( blastRadius, blastRadius );
          world->QueryAABB( &queryCallback, aabb );
  
          //check which of these bodies have their center of mass within the blast radius
          for (int i = 0; i < queryCallback.foundBodies.size(); i++) {
              b2Body* body = queryCallback.foundBodies[i];
              b2Vec2 bodyCom = body->GetWorldCenter();
      
              //ignore bodies outside the blast range
              if ( (bodyCom - center).Length() >= blastRadius )
                  continue;
          
              queryCallback.applyBlastImpulse(body, center, bodyCom, blastPower );
          }
			  }
        
        if(scale > 0.1f) {
          b->spawnExplosion(scale);
        }
				b->destroyPhysics();
				b->removeFromParent();
				//delete b;
      }
    }
	}
    
    for(PlayerManager::TPlayerMap::iterator it = PlayerManager::instance().m_players.begin();
        it != PlayerManager::instance().m_players.end(); ++it)
    {
        Player * player = it->second;
        player->postPhysicsUpdate();
    }

    if(Input::instance()->keyPressed('U') && tutorial)
    {
        tutorial->runAction(CCSequence::create(CCFadeOut::create(0.5f), NULL));
        tutorial = NULL;
    } else if(Input::instance()->keyPressed('T') && tutorial == NULL)
    {
        tutorial = CCSprite::create("tutorial.png");
        tutorial->runAction(CCJumpTo::create(0.8, 
            ccp(getContentSize().width / 2, getContentSize().height / 2), 100, 3));
        addChild(tutorial, 1);
    }
    
    contactListener->invokePowerUps();
}

void GameScene::DrawBox2DDebugData()
{
	kmGLPushMatrix();
	world->DrawDebugData();
	kmGLPopMatrix();
}

void GameScene::initBox2DDebugDraw()
{
	GLESDebugDraw* debugDraw = new GLESDebugDraw( PTM_RATIO );
	world->SetDebugDraw(debugDraw);

	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	flags += b2Draw::e_jointBit;
	flags += b2Draw::e_aabbBit;
	flags += b2Draw::e_pairBit;
	flags += b2Draw::e_centerOfMassBit;
	debugDraw->SetFlags(flags);
}

void GameScene::initMap(int levelNumber)
{
    map = Map::create(levelNumber, world);
    addChild(map);
}

void GameScene::initHud()
{
	hud = Hud::create();
	addChild(hud);
}

void GameScene::initItemSpawner()
{
//    spawnTimer = CCTimer::timerWithTarget(this, , 5.0);
    
    getScheduler()->scheduleSelector(schedule_selector(GameScene::updatePowerUps), this, 5.0, -1, 0, false);
}

static void player_add_points(Player * p)
{
    p->addHP(10);
    p->addAchievement(GotPotion);
}

static void player_sub_points(Player * p)
{
    p->addScore(-500);
    p->addAchievement(WatchOutAchievement);
}

void GameScene::updatePowerUps(float dt)
{
    printf("Update powerups %f", dt);
    
    int num_pups = 0;
    
    CCArray * children = this->getChildren();
    for(int i = 0 ; i < children->count() ; ++i) {
        if(dynamic_cast<PowerUp *>(children->objectAtIndex(i))) {
            num_pups++;
            if(num_pups >= 5)
                break;
        }
    }
    
    if(num_pups < 5) {
        // Spawn powerups
        
        CCSize size = getContentSize();
        
        int x = rand() % (int)size.width;
        int y = rand() % (int)size.height;
        
        PowerUp * pup;
        
        int prob = rand() % 1000;
        
        if(prob < 500)
            pup = PowerUp::create("health.png", &player_add_points);
        else
            pup = PowerUp::create("skul.png", &player_sub_points);
        
        pup->setupPhisics(world, x, y);
        
//        printf("spawn powerup at x=%d y=%d\n", x, y);
        
        pup->setPosition(CCPointMake(x, y));
        
        addChild(pup);
    }
}

void GameScene::updateNetwork(float delta)
{
  const float TIME_SLOWDOWN = 0.4f;

    // Save current as previous
    for(PlayerManager::TPlayerMap::iterator it = PlayerManager::instance().m_players.begin();
        it != PlayerManager::instance().m_players.end(); ++it)
    {
        Player * player = it->second;
        player->prev = player->cur;
    }

    // Update players
    std::vector<NetworkPacket> packets(NetworkThread::instance().getPackets());

    std::set<unsigned int> playersSet;
    
    for(std::vector<NetworkPacket>::reverse_iterator it = packets.rbegin() ; it != packets.rend() ; ++it) {
        const NetworkPacket& packet = *it;
        if(playersSet.find(packet.player) == playersSet.end()) {
            Player * player = PlayerManager::instance().getPlayer(packet.player);
            if(player == NULL) {
                player = createPlayer(packet);
                player->cur = player->prev = packet;
            } else {
                player->cur = packet;
            }
        }
    }

	bool timeSlowdown = false;

    // Process PLAYER updates
    for(PlayerManager::TPlayerMap::iterator it = PlayerManager::instance().m_players.begin();
        it != PlayerManager::instance().m_players.end(); ++it)
    {
        Player * player = it->second;

        // Apply new force
        player->updateJoyPosition(player->cur.x, player->cur.y);

        // TODO: support for firing and other events
		if(player->cur.b1 && !player->prev.b1)
		{
      if(player->mana > 5)
      {
        player->mana -= 5;
        player->fire(BT_Fire);
      }
		}

		if(player->cur.b2 && !player->prev.b2)
		{
      if(player->mana > 15)
      {
        player->mana -= 15;
			  player->fire(BT_Rocket);
      }
		}

    if(player->cur.b3 && !player->prev.b3)
    {
      if(player->mana > 70)
      {
        player->mana -= 70;
        player->fire(BT_Bomb);
      }
    }

    if(player->cur.b4 && !player->prev.b4)
    {
      player->addHP(-100);
    }

		if(player->cur.b2)
		{
			// CHECK MP

      player->mana -= 100/(3) * delta;

      if(player->mana < 10) {
        player->mana = 0;
      } else {
			  Bullet* bullet = findLatestBullet(player, BT_Rocket);
			  if(bullet)
			  {
				  b2Body* body = bullet->getB2Body();

          float currentAngle = body->GetAngle();
          float rotateAngle = 0.0f;

          b2Vec2 xy = body->GetLinearVelocity();

				  float factor = fabs(player->cur.ay) * 10000;

				  if(fabsf(player->cur.ay) > 0.2f)
            rotateAngle = 3 * M_PI * player->cur.ay * delta;
          
          float cs = cos(rotateAngle);
          float sn = sin(rotateAngle);

          float x = xy.x * cs - xy.y * sn;
          float y = xy.x * sn + xy.y * cs;

          float realAngle = atan2f(y, x) - M_PI / 2 ;

				  body->SetTransform(body->GetWorldCenter(), realAngle);
          body->SetLinearVelocity(b2Vec2(x, y));
			  }
			
			  timeSlowdown = true;
      }
		} else {
      player->mana += 100/(10*TIME_SLOWDOWN) * delta;
      if(player->mana > 100)
        player->mana = 100;
    }
    }

	if(timeSlowdown)
		expectedTimeScale = TIME_SLOWDOWN;
	else
		expectedTimeScale = 1.0f;
}

Player* GameScene::createPlayer(const NetworkPacket &packet )
{
    Player* player = Player::create();
    player->m_playerId = packet.player;
	hud->createHudForPlayer(player);

    player->setAnchorPoint(ccp(0.5f, 0.5f));
    player->setupPhysics(world);
    
	player->getB2Body()->SetAngularDamping(10);
	player->getB2Body()->SetLinearDamping(10);

	b2MassData mass = {100, b2Vec2(0,0), 100.0f};
	player->getB2Body()->SetMassData(&mass);

    // set spawn point
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    player->setPosition(map->spawPoints[rand() % map->spawPoints.size()]);
    
    PlayerManager::instance().addPlayer(player);
    map->addChild(player);
	player->startMotionStreak();

    return player;
}

void GameScene::destroyBlockAtPosition( CCPoint position )
{
    CCArray* children = this->map->getChildren();
    for(int i = 0; i < children->count(); ++i) {
        Block* singleBlock = (Block*)children->objectAtIndex(i);
        if(singleBlock->getTag() != 5) {
            continue;
        }
        CCPoint location = this->convertToNodeSpace(position);
        CCRect rect = singleBlock->boundingBox();
        if(singleBlock->boundingBox().containsPoint(location))
        {
            singleBlock->destroy();
            break;
        }
    }
}

void GameScene::checkKeyboardKeys()
{
	int mapLevel = 0;
#ifdef _WIN32
	if(Input::instance()->keyDown(VK_F1)) {
		mapLevel = 1;
	}
	else if(Input::instance()->keyDown(VK_F2)) {
		mapLevel = 2;
	}
	else if(Input::instance()->keyDown(VK_F3)) {
		mapLevel = 3;
	}
	else if(Input::instance()->keyDown(VK_F4)) {
		mapLevel = 4;
	}
#elif defined(__APPLE__)
    if(Input::instance()->keyDown('1')) {
		mapLevel = 1;
	}
	else if(Input::instance()->keyDown('2')) {
		mapLevel = 2;
	}
	else if(Input::instance()->keyDown('3')) {
		mapLevel = 3;
	}
	else if(Input::instance()->keyDown('4')) {
		mapLevel = 4;
	}
#endif
    
	if(mapLevel != 0) {
		CCDirector::sharedDirector()->pushScene(GameScene::scene(mapLevel));
	}
}

void GameScene::playerKilledPlayer(Player * killer, Player * killed)
{
    if(totalKills == 0) {
        ++totalKills;
        ++killer->killsInRow;
        lastKiller = killer;
        killer->addAchievement(FirstBlood);
    } else if(lastKiller == killer) {
        ++totalKills;
        ++killer->killsInRow;
        if(killer->killsInRow == 2) {
            killer->addAchievement(DoubleKill);
        } else {
            killer->addAchievement(MultiKill);
        }
    } else {
        if(lastKiller) {
            lastKiller->killsInRow = 0;
        }
        
        killer->killsInRow = 1;
        
        lastKiller = killer;
        ++totalKills;
    }
}

//// ---------------------------------------------------------- Tests ---------------------------------------------------------------
//
//void GameScene::initTests( CCSize &visibleSize, CCPoint &origin )
//{
//	///////////////////////////////
//	//// 3. add your codes below...
//
//	//// add a label shows "Hello World"
//	//// create and initialize a label
//	//
//	//CCLabelTTF* pLabel = CCLabelTTF::create("Hello World", "Arial", 24);
//	//
//	//// position the label on the center of the screen
//	//pLabel->setPosition(ccp(origin.x + visibleSize.width/2,
//	//                        origin.y + visibleSize.height - pLabel->getContentSize().height));
//
//	//// add the label as a child to this layer
//	//this->addChild(pLabel, 1);
//
//	CCParticleSystem* lines = CCParticleSystemQuad::create("lines.plist");
//	lines->retain();
//	lines->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
//
//	this->addChild(lines, 1);
//
//	CCDirector::sharedDirector()->getScheduler()->scheduleSelector(
//		schedule_selector(GameScene::fire), 
//		this,
//		0.15f, 
//		false);
//}
//
//void GameScene::fire(float dt)
//{
//	CCParticleSystem* fire = CCParticleSystemQuad::create("ExplodingRing.plist");
//	fire->setPosition(getContentSize().width / 2, getContentSize().height / 2);
//	fire->setAutoRemoveOnFinish(true);
//	this->addChild(fire, 1);
//}

Bullet* GameScene::findLatestBullet(Player* owner, BulletType type) {
	b2Body* first = world->GetBodyList();
	while(first)
	{
		b2Body* current = first;
		first = first->GetNext();

		CCPhysicsSprite* sprite = (CCPhysicsSprite*)current->GetUserData();
		if(!sprite)
			continue;

		if(Bullet * b = dynamic_cast<Bullet*>(sprite)) {
			if(b->owner == owner && b->bulletType == type) {
				return b;
			}
		}
	}
    return NULL;
}
