#include "StdAfx.h"
#include "AppDelegate.h"
#include "SplashScreenScene.h"
#include "GameScene.h"
#include "GameOverScene.h"
#include "AboutScene.h"
#include "MenuScene.h"
#include "NetworkThread.h"
#include "PathUtils.h"
#include "IGKEAGLView.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    CCDirector* pDirector = CCDirector::sharedDirector();
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

    pDirector->setOpenGLView(pEGLView);
	
    // turn on display FPS
    //pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    PathUtils::init();
    
    NetworkThread::instance();
    
    // run
	pDirector->runWithScene(SplashScreenScene::scene());
	//pDirector->runWithScene(MenuScene::scene());
//	pDirector->runWithScene(GameScene::scene(1));
	//pDirector->runWithScene(AboutScene::scene());
	//pDirector->runWithScene(GameOverScene::scene());

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
