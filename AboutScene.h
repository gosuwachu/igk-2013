#pragma once
#include "cocos2d.h"

USING_NS_CC;

class AboutScene : public CCLayer
{
public:
	AboutScene(void);
	~AboutScene(void);

	CREATE_FUNC(AboutScene);
	static CCScene* scene();

	virtual void ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent );

	virtual bool init();

};

