//
//  main.m
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 06.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
