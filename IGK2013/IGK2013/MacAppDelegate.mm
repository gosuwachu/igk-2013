/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "MacAppDelegate.h"
#import "AppDelegate.h"
#import "Input.h"
#import <ctype.h>
#import "NetworkThread.h"

static AppDelegate s_sharedApplication;

@interface EAGLViewKeys : EAGLView {
    NetworkPacket netPacket, netPacket2;
}
@end

@implementation EAGLViewKeys

- (id)initWithFrame:(NSRect)frameRect {
    if(self = [super initWithFrame:frameRect]) {
        memset(&netPacket, 0, sizeof(netPacket));
        netPacket.player = 1;
        memset(&netPacket2, 0,sizeof(netPacket2));
        netPacket2.player = 2;
    }
    return self;
}

- (void)keyDown:(NSEvent *)theEvent {
    [super keyDown:theEvent];
    
    NSString * c0 = [theEvent characters];
    if(c0 != nil && [c0 length] == 1) {
        Input::instance()->setKey(toupper([c0 characterAtIndex:0]), true);
     
        unichar k = [c0 characterAtIndex:0];
        [self updateKey:k pressed:YES];
    } else {
        Input::instance()->setKey([theEvent keyCode], true);
    }
}

- (void)keyUp:(NSEvent *)theEvent {
    [super keyUp:theEvent];
    NSString * c0 = [theEvent characters];
    if(c0 != nil && [c0 length] == 1) {
        Input::instance()->setKey(toupper([c0 characterAtIndex:0]), false);
        
        unichar k = [c0 characterAtIndex:0];
        [self updateKey:k pressed:NO];
    } else {
        Input::instance()->setKey([theEvent keyCode], false);
    }
}

- (void)updateKey:(unichar)key pressed:(BOOL)pressed {
    bool p1=false,p2=false;
    
    if(key == 'w' || key == 'W') {
        p1=true;
        if(pressed) {
            if(netPacket.y >= 0) {
                netPacket.y = -1;
            }
        } else {
            if(netPacket.y < 0) {
                netPacket.y = 0;
            }
        }
    } else if(key == 's' || key == 'S') {
        p1=true;
        if(pressed) {
            if(netPacket.y <= 0) {
                netPacket.y = 1;
            }
        } else {
            if(netPacket.y > 0) {
                netPacket.y = 0;
            }
        }
    } else if(key == 'd' || key == 'D') {
        p1=true;
        if(pressed) {
            if(netPacket.x <= 0) {
                netPacket.x = 1;
            }
        } else {
            if(netPacket.x > 0) {
                netPacket.x = 0;
            }
        }
    } else if(key == 'a' || key == 'A') {
        p1=true;
        if(pressed) {
            if(netPacket.x >= 0) {
                netPacket.x = -1;
            }
        } else {
            if(netPacket.x < 0) {
                netPacket.x = 0;
            }
        }
    } else if(key == ' ') {
        p1=true;
        netPacket.b1 = pressed ? 1 : 0;
    } else if(key == 'z' || key == 'Z') {
        p1=true;
        netPacket.b2 = pressed ? 1 : 0;
    } else if(key == 'X' || key == 'X') {
        p1=true;
        netPacket.b3 = pressed ? 1 : 0;
    } else if(key == 'V' || key == 'v') {
        p1=true;
        netPacket.b4 = pressed ? 1 : 0;
    }

    
    if(key == 'i' || key == 'I') {
        p2 = true;
        if(pressed) {
            if(netPacket2.y >= 0) {
                netPacket2.y = -1;
            }
        } else {
            if(netPacket2.y < 0) {
                netPacket2.y = 0;
            }
        }
    } else if(key == 'k' || key == 'K') {
        p2 = true;
        if(pressed) {
            if(netPacket2.y <= 0) {
                netPacket2.y = 1;
            }
        } else {
            if(netPacket2.y > 0) {
                netPacket2.y = 0;
            }
        }
    } else if(key == 'l' || key == 'L') {
        p2 = true;
        if(pressed) {
            if(netPacket2.x <= 0) {
                netPacket2.x = 1;
            }
        } else {
            if(netPacket2.x > 0) {
                netPacket2.x = 0;
            }
        }
    } else if(key == 'j' || key == 'J') {
        p2 = true;
        if(pressed) {
            if(netPacket2.x >= 0) {
                netPacket2.x = -1;
            }
        } else {
            if(netPacket2.x < 0) {
                netPacket2.x = 0;
            }
        }
    } else if(key == '\r' || key == '\n') {
        p2 = true;
        netPacket2.b1 = pressed ? 1 : 0;
    } else if(key == ',' || key == '<') {
        p2 = true;
        netPacket2.b2 = pressed ? 1 : 0;
    } else if(key == '.' || key == '>') {
        p2 = true;
        netPacket2.b3 = pressed ? 1 : 0;
    } else if(key == '/' || key == '?') {
        p2 = true;
        netPacket2.b4 = pressed ? 1 : 0;
    }
    
    netPacket.touch = true;
    netPacket2.touch = true;
    
//    printf("P X=%f Y=%f B1=%d B2=%d B3=%d B4=%d\n", netPacket.x, netPacket.y, netPacket.b1, netPacket.b2, netPacket.b3, netPacket.b4);
    
    if(p1)
        NetworkThread::instance().pushPacket(netPacket);
    if(p2)
        NetworkThread::instance().pushPacket(netPacket2);
}

@end

@implementation AppController

@synthesize window, glView;

-(void) applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // create the window
    // note that using NSResizableWindowMask causes the window to be a little
    // smaller and therefore ipad graphics are not loaded
    NSRect rect = NSMakeRect(0, 0, 2048, 1536);
    window = [[NSWindow alloc] initWithContentRect:rect
                                         styleMask:( NSClosableWindowMask | NSTitledWindowMask )
                                           backing:NSBackingStoreBuffered
                                             defer:YES];
    
    // allocate our GL view
    // (isn't there already a shared EAGLView?)
    glView = [[EAGLViewKeys alloc] initWithFrame:rect];
    
    // set window parameters
    [window becomeFirstResponder];
    [window setContentView:glView];
    [window setTitle:@"Tank Destroyer by Kryzys IGK 2013"];
    [window makeKeyAndOrderFront:self];
    [window setAcceptsMouseMovedEvents:NO];
    
    [glView setFrameZoomFactor:0.4];
    
    cocos2d::CCApplication::sharedApplication()->run();
}

-(BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)theApplication
{
    return YES;
}

-(void) dealloc
{
    cocos2d::CCDirector::sharedDirector()->end();
    [super dealloc];
}

#pragma mark -
#pragma mark IB Actions

-(IBAction) toggleFullScreen:(id)sender
{
    EAGLView* pView = [EAGLView sharedEGLView];
    [pView setFullScreen:!pView.isFullScreen];
}

-(IBAction) exitFullScreen:(id)sender
{
    [[EAGLView sharedEGLView] setFullScreen:NO];
}

@end
