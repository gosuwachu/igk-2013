#ifndef __IGK2013__PathUtils__
#define __IGK2013__PathUtils__

#include <string>

class PathUtils
{
public:
    static std::string resourcePath(const std::string& fileName);
    static void init();
};

#endif /* defined(__IGK2013__PathUtils__) */
