#pragma once
#include "cocos2d.h"

USING_NS_CC;

class SplashScreenScene : public CCLayerColor
{
public:
	SplashScreenScene(void);
	~SplashScreenScene(void);

	CREATE_FUNC(SplashScreenScene);

	static CCScene* scene();

	virtual bool init();

	virtual void ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent );

};

